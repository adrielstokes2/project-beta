# CarCar

Team:

* Daniel Gettinger - service
* Adriel Stokes - sales

## Instructions to run the project:
Make sure docker, git, and Node.js 18.2 are installed and up-to-date.

1. Fork this repository

2. Clone the forked repository onto your local computer:
git clone <<respository.url.here>>

3. Build and run the project using Docker with these commands:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- After running these commands, make sure all of your Docker containers are running

- View the project in the browser: http://localhost:3000/

![Img](/images/CarCarWebsite.png)

## Design/Diagram:

Sales                           Inventory                   Service
 _______________                _____________              _____________
|-AutomobileVO |               |             |            |-Technician  |
|-Salesperson  |  <----------  |-Manufacturer| ---------> |-AutomobileVO|
|-Customer     |               |-VehicleModel|            |-Appointment |
|-Sale         |               |-Automobile  |            |             |
|______________|               |_____________|            |_____________|


## API Documentation:

High Level Overview:
There are three main API's for this project: Service, Sales, and Inventory. These three microservices together
employ domain driven design and are designed to be able to stand on their own independently, but also to work in
unison. Sales and Service use pollers to pull the automobile information from the inventory microservice, allowing
them to keep up to date information about which cars have been sold or are being worked on. All three microservices
utalize their own ports and paths.


## Inventory API:
The Inventory API microservice enables the sales and service teams to pull Automobile information and
populate Automobile Value Object instances for each microservice to work with independently. This
microservice came fully packaged and neede 0 editing, and serves as the aggregate from which the other
two can pull down value objects.

URLs and Ports:

| Action | Method | URL
| ----------- | ----------- | ----------- |

| List manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/id/
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/id/
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/id/

| List vehicle models | GET | http://localhost:8100/api/models/
| Create a vehicle model | POST | http://localhost:8100/api/models/
| Get a specific vehicle model | GET | http://localhost:8100/api/models/id/
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/id/
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/id/




- The **'vin'** at the end of the detail urls represents the VIN for the specific automobile you want to access. This is not an integer ID. This is a string value so you can use numbers and/or letters.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create an automobile | POST | http://localhost:8100/api/automobiles/
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/vin/
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/vin/
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/vin/




### Sales API:


URLs and Ports:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salespeople | GET | http://localhost:8090/api/salespeople/
| Create a salesperson | POST | http://localhost:8090/api/salespeople/
| Delete a salesperson | DELETE | http://localhost:8090/api/salesperson/:id/

| List customers | GET | http://localhost:8090/api/customers/
| Create a customer | GET | http://localhost:8090/api/customers
| Delete a specific customer | DELETE | http://localhost:8090/api/customers/:id/

- the id value to show a salesperson's salesrecord is the **"id" value tied to a salesperson.**

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List all sales | GET | http://localhost:8090/api/sales/
| Create a new sale | POST | http://localhost:8090/api/sales/
| Show salesperson's sales | GET | http://localhost:8090/api/sales/:id/

## Services Ports and URLs
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:8080/api/models/technicians/
| Create a technician | POST | http://localhost:8080/api/technicians/
| Delete a specific technician | DELETE | http://localhost:8080/api/technicians/:id/

| List appointments | GET | http://localhost:8080/api/appointments/
| Create an appointment | POST | http://localhost:8080/api/appointments/
| Delete an appointment | DELETE | http://localhost:8080/api/appointments/:id/
| Set appointment status to "canceled"  | PUT | http://localhost:8080/api/appointments/:id/cancel/
| Set appointment status to "finished" | PUT| http://localhost:8080/api/appointments/:id/finish/


## Service microservice

The service microservice has 3 models: Technician, AutomobileVO, and Appointment. The Appointment model interactas with the Technician model, receiving information from that model.

The AutomobileVO is a value object that gets data about the automobiles in the inventory using a poller. The service poller automatically polls the inventory microservice for data, so the service microservice is constantly getting the updated data.

These microservices are integrated because, when registering a service, a specific automobile needs to be selected, and that information lives inside the inventory microservice.

## Sales microservice
On the backend, the sales microservice has 4 models: AutomobileVO, Customer, Salesperson, and Sale. Sale is the model that interacts with the other three models. This model gets data from the three other models.

The AutomobileVO is a value object that gets data about the automobiles in the inventory using a poller. The sales poller automotically polls the inventory microservice for data, so the sales microservice is constantly getting the updated data.

The reason for integration between these two microservices is that when recording a new sale, you'll need to choose which car is being sold and that information lives inside of the inventory microservice.
