import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here. Ignore vs-code error hinting
# from service_rest.models import Something
#http://project-beta-inventory-api-1:8000/api/automobiles/
# http://inventory-api:8100/api/services/vin/
from service_rest.models import AutomobileVO


def get_automobile():
    response = requests.get('http://project-beta-inventory-api-1:8000/api/automobiles/')
    content = json.loads(response.content)
    for auto in content["autos"]:
        AutomobileVO.objects.update_or_create(
            vin=auto["href"],
            defaults={"sold": auto["sold"]
                      }) 

# "autos": [
#                 {"href": "/api/automobiles/1/", "vin": "1", "sold": True},
#                 {"href": "/api/automobiles/2/", "vin": "2", "sold": True},
#                 {"href": "/api/automobiles/3/", "vin": "3", "sold": True},
#             ]
def poll(repeat=True):
    while True:
        print('Service poller polling for data')
        try:
            # Write your polling logic, here
            # Do not copy entire file
            get_automobile()
            print("is working")
        except Exception as e:
            print(e, file=sys.stderr)

        if (not repeat):
            break

        time.sleep(60)


if __name__ == "__main__":
    poll()
