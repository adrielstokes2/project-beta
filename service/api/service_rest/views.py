from django.shortcuts import render
from .models import Technician, AutomobileVO, Appointment
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
import json


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
        "id",
                ]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name",
                  "last_name",
                  "employee_id",
                  "id",
                  ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ["date_time",
                  "reason",
                  "status",
                  "customer",
                  "technician",
                  "vin",
                  "vip_status",
                  "id",
                  ]
    encoders = {"technician": TechnicianListEncoder(),
                }

    def get_extra_data(self, o):
        count = AutomobileVO.objects.filter(vin=o.vin).count()
        return {"vip_status": count > 0}



# Create your views here.
@require_http_methods(["GET", "POST"])
def api_list_technicians(request):

    if request.method == "GET":
        # if bin_vo_id is not None:
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
            safe=False
        )
    else:
        try:
            content = json.loads(request.body)
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"message": "failed to make technician"}
            )
            response.status_code = 400
            return response
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False,
        )



@require_http_methods(["DELETE", "GET", "PUT"])
def api_delete_technician(request, pk):
    if request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianListEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response=JsonResponse(
                {"message": "Failed technician deletion"}
            )
            response.status_code=404
            return response
        
# @require_http_methods(["PUT", "GET"])
# def api_finish_appointment(request, id):
#     if request.method == "PUT":
#         content = json.loads(request.body)
#         try:
#             if "status" in content:
#                 status = Appointment.objects.get(id=id)
#                 content["status"] = status
#         except Appointment.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid status"},
#                 status=400,
#             )
#         Appointment.objects.filter(id=id).update(**content) #id or pk?
#         appointment = Appointment.objects.get(id=id)
#         return JsonResponse(
#             appointment,
#             encoder=AppointmentListEncoder, #new encoder?
#             safe=False,
#         )

# @require_http_methods(["PUT", "GET"])
# def api_cancel_appointment(request, id):
#     if request.method == "PUT":
#         content = json.loads(request.body)
#         try:
#             if "status" in content:
#                 status = Appointment.objects.get(content["status"])
#                 content["status"] = status
#         except Appointment.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid status"},
#                 status=400,
#             )
#         Appointment.objects.filter(id=id).update(**content) #id or pk?
#         appointment = Appointment.objects.get(id=id)
#         return JsonResponse(
#             appointment,
#             encoder=AppointmentListEncoder, #new encoder?
#             safe=False,
#         )

# @require_http_methods(["GET"])
# def api_list_upcoming_service_appointments(request):
#     if request.method == "GET":
#         services = ServiceAppointment.objects.filter(status="SCHEDULED")
#         return JsonResponse(
#             {"services": services},
#             encoder=ServiceAppointmentEncoder,
#         )



@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(employee_id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "technician id doesn't exist"},
                status=400,
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )

# @require_http_methods(["PUT"])
# def api_finish_appointment(request, pk):
#     content = json.loads(request.body)
#     try:
#         if "status" in content:
#             status = Appointment.objects.get(content["status"])
#             content["status"] = status
#     except Appointment.DoesNotExist:
#         return JsonResponse(
#             {"message": "failed to update"},
#             status=400,
#         )
#     Appointment.objects.filter(id=pk).update(**content)
#     appointment = Appointment.objects.get(id=pk)
#     return JsonResponse(
#         appointment,
#         encoder=AppointmentListEncoder,
#         safe=False
#     )



@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    appointment = Appointment.objects.get(id=pk)
    appointment.finish()
    return JsonResponse(
        appointment,
        encoder=AppointmentListEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    appointment = Appointment.objects.get(id=pk)
    appointment.cancel()
    return JsonResponse(
        appointment,
        encoder=AppointmentListEncoder,
        safe=False,
    )




@require_http_methods(["DELETE"])
def api_delete_appointment(request, pk):
    if request.method == "DELETE":
        appointment = Appointment.objects.get(id=pk)
        appointment.delete()
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
            )
