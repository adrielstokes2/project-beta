import React, {useState, useEffect } from 'react';

function VehicleModelForm () {

  const [formData, setFormData] = useState({
    name: '',
    picture_url: '',
    manufacturer: '',
  })
  const [hasVehicleModel, setHasVehicleModel] = useState(false)

  const handleSubmit = async (event) => {
    event.preventDefault();
    const locationUrl = `http://localhost:8100/api/models/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      setFormData({
        name: '',
        picture_url: '',
        manufacturer: ''
      });
      setHasVehicleModel(true);
    }
  }
  const handleChangeName = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    });
  }
  //Allows for the form to display on when a user has not signed in.
  //If a user has signed in, the css class d-none is added to the unneeded piece of the interface
   const formClasses = (!hasVehicleModel) ? '' : 'd-none';
   const messageClasses = (!hasVehicleModel) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';
  return (
    <div className="my-5">
      <div className="row">
        {/* <div className="col col-sm-auto">
          <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="logo.svg" />
        </div> */}
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-technician-form">
                <h1 className="card-title">It's auto Time!</h1>
                <p className="mb-3">
                  Please add a vehichle model.
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.name} required placeholder="name" type="text" id="name" name="name" className="form-control" />
                      <label htmlFor="model_name">Name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.picture_url} required placeholder="picture_url" type="text" id="picture_url" name="picture_url" className="form-control" />
                      <label htmlFor="color">Picture</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.manufacturer} required placeholder="manufacturer" type="text"  id="manufacturer" name="employee_id" className="form-control" />
                      <label htmlFor="manufacturer">Choose a Manufacturer</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create</button>
              </form>
              <div className={messageClasses} id="success-message">
                {/* code is good at this message */}
                Vehicle Model added!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default VehicleModelForm;
