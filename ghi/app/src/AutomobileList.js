import { useEffect, useState } from 'react';

function AutomobileList() {
  const [technicians, setTechnicians] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8100/api/automobiles/');

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians)
    }
  }

  useEffect(()=>{
    getData()
  }, [])
  
//   const handleDelete = async (id) => { 
//     await fetch(`http://localhost:8080/api/technicians/${id}/`, {method: "DELETE"})
//     await getData()
//   }
  
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Employee ID</th>
        </tr>
      </thead>
      <tbody>
        {technicians.map((technician, idx) => {
          return (
            <tr>
              <td>{ automobile.year }</td>
              <td>{ automobile.vin }</td>
              <td>{ automobile.sold}</td>
              <td>{ automobile.color}</td>
              <td>{ automobile.model.manufacturer}</td>
            </tr>
            
          );
        })}
      </tbody>
    </table>
  );
}

export default AutomobileList;
