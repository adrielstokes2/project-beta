import { useEffect, useState } from 'react';

function ServiceHistory() {
  const [appointments, setAppointments] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8080/api/appointments/');

    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments)
    }
  }

  useEffect(()=>{
    getData()
  }, [])
  
//   const handleDelete = async (id) => { 
//     await fetch(`http://localhost:8080/api/technicians/${id}/`, {method: "DELETE"})
//     await getData()
//   }
  
  return (
    
    <table className="table table-striped">
      <thead>
      <div className="col">
                    <div className="form-floating mb-3">
                      <input  required placeholder="last name" type="text" id="last_name" name="last_name" className="form-control" />
                      <label htmlFor="color">search by vin</label>
                      <button className="btn btn-lg btn-primary">search</button>
                    </div>
                  </div>
        <tr>
        
          <th>VIN</th>
          <th>VIP status</th>
          <th>Customer</th>
          <th>Reason</th>
          <th>Date&Time</th>
          <th>name</th>
          <th>status</th>
        </tr>
      </thead>
      <tbody>
        {appointments.map((appointment, idx) => {
          return (
            <tr key={appointment.id}>
              <td>{ appointment.vin}</td>
              <td>{ appointment.vip_status}</td>
              <td>{ appointment.customer}</td>
              <td>{ appointment.reason }</td>
              <td>{ appointment.date_time}</td>
              <td>{ appointment.technician.first_name } {appointment.technician.last_name}</td>
              <td>{ appointment.status}</td>
              
            </tr>
            
          );
        })}
      </tbody>
    </table>
  );
}

export default ServiceHistory;
