import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ListSalespeople from './SalespeopleList';
import TechnicianList from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import CustomerList from './CustomerList';
import SalespersonForm from './SalespersonForm';
import CustomerForm from './CustomerForm';
import SalesList from './SalesList';
import VehicleModelList from './VehicleModelList';
import VehicleModelForm from './VehicleModelForm';
import ServiceHistory from './ServiceHistory';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import AutomobileList from './AutomobileList';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="salespersonlist/" element={<ListSalespeople />} />
          <Route path="customerlist/" element={<CustomerList />} />
          <Route path="salesperson/new/" element={<SalespersonForm />} />
          <Route path="customer/new/" element={<CustomerForm />} />
          <Route path="saleslist/" element={<SalesList />} />
          <Route path="vehicle_model_list/" element={<VehicleModelList />} />
          <Route path="vehicle_model/new/" element={<VehicleModelForm />} />
          <Route path="technicians/" element={<TechnicianList />} />
          <Route path="technicians/new/" element={<TechnicianForm />} />
          <Route path="appointments/" element={<AppointmentList />} />
          <Route path="appointments/new/" element={<AppointmentForm />} />
          <Route path="appointments/filtered/" element={<ServiceHistory />} />
          <Route path="manufacturers/" element={<ManufacturerList />} />
          <Route path="manufacturers/new" element={<ManufacturerForm />} />
          <Route path="automobiles/" element={<AutomobileList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
