import { useEffect, useState } from 'react';

function VehicleModelList() {
  const [vehicleModels, setVehicleModels] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8100/api/models/');

    if (response.ok) {
      const data = await response.json();
      setVehicleModels(data.models)
    }
  }

  useEffect(()=>{
    getData()
  }, [])


  return (
    <>
    <h1>Models</h1>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Vehicle Model Name</th>
          <th>Manufacturer</th>
          <th>Picture</th>
        </tr>
      </thead>
      <tbody>
        {vehicleModels.map((models, idx) => {
          return (
            <tr>
              <td>{ models.name }</td>
              <td>{ models.manufacturer.name }</td>
              <td>{ models.picture_url}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
    </>
  );
}

export default VehicleModelList;
