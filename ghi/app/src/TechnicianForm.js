import React, {useState} from 'react';

//const location, setlocations
//hats after use effect and get data
function TechnicianForm() {
  const [formData, setFormData] = useState({
    first_name: '',
    last_name: '',
    employee_id: ''
  })

  const [hasTechnician, setHasTechnician] = useState(false)

//   const getData = async () => {
//     const url = 'http://localhost:8100/api/automobiles/';
//     const response = await fetch(url);

//     if (response.ok) {
//       const data = await response.json();
//       setAutos(data.autos);
//     }
//   }
  
//   useEffect(() => {
//     getData();
//   }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    
    const locationUrl = `http://localhost:8080/api/technicians/`;
    
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    
    if (response.ok) {
      setFormData({
        first_name: '',
        last_name: '',
        employee_id: ''
      });

      setHasTechnician(true);
    }
  }

  const handleChangeName = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value 
    });
  }

  //Allows for the form to display on when a user has not signed in.
  //If a user has signed in, the css class d-none is added to the unneeded piece of the interface

   const formClasses = (!hasTechnician) ? '' : 'd-none';
   const messageClasses = (!hasTechnician) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';
  
  return (
    <div className="my-5">
      <div className="row">
        {/* <div className="col col-sm-auto">
          <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="logo.svg" />
        </div> */}

        <div className="col">
          <div className="card shadow">
            <div className="card-body">

              <form className={formClasses} onSubmit={handleSubmit} id="create-technician-form">
                <h1 className="card-title">Create technician</h1>
                <p className="mb-3">
                 
                </p>
                
                {/* <div className="mb-3">
                  <select onChange={handleChangeName} name="auto" id="auto" required>
                    <option value="">Choose a vin</option>
                    {
                      autos.map(auto => {
                        return (
                          <option key={auto.href} value={auto.href}>{auto.sold}</option>
                        )
                      })
                    }
                  </select>
                </div> */}

                <p className="mb-3">
                  Describe the technician.
                </p>

                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.first_name} required placeholder="first name" type="text" id="first_name" name="first_name" className="form-control" />
                      <label htmlFor="model_name">first name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.last_name} required placeholder="last name" type="text" id="last_name" name="last_name" className="form-control" />
                      <label htmlFor="color">last name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.employee_id} required placeholder="employee id" type="text"  id="employee_id" name="employee_id" className="form-control" />
                      <label htmlFor="picture_url">employee id</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create</button>
              </form>
              

              <div className={messageClasses} id="success-message">
                Technician added!
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default TechnicianForm;
