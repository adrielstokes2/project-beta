import React, {useState, useEffect } from 'react';

function CustomerForm() {

  const [formData, setFormData] = useState({
    first_name: '',
    last_name: '',
    phone_number: '',
    address: ''
  })
  const [hasCustomer, setHasCustomer] = useState(false)

  const handleSubmit = async (event) => {
    event.preventDefault();
    const locationUrl = `http://localhost:8090/api/customers/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      setFormData({
        first_name: '',
        last_name: '',
        phone_number: '',
        address: ''
      });
      setHasCustomer(true);
    }
  }
  const handleChangeName = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    });
  }
  //Allows for the form to display on when a user has not signed in.
  //If a user has signed in, the css class d-none is added to the unneeded piece of the interface
   const formClasses = (!hasCustomer) ? '' : 'd-none';
   const messageClasses = (!hasCustomer) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';
  return (
    <div className="my-5">
      <div className="row">
        {/* <div className="col col-sm-auto">
          <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="logo.svg" />
        </div> */}
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-technician-form">
                <h1 className="card-title">It's auto Time!</h1>
                <p className="mb-3">
                  Please add a customer
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.first_name} required placeholder="first name" type="text" id="first_name" name="first_name" className="form-control" />
                      <label htmlFor="model_name">First name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.last_name} required placeholder="last name" type="text" id="last_name" name="last_name" className="form-control" />
                      <label htmlFor="color">Last name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.phone_number} required placeholder="Customer phone number" type="text"  id="phone_number" name="phone_number" className="form-control" />
                      <label htmlFor="picture_url">Customer phone number</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.address} required placeholder="Address" type="text"  id="address" name="address" className="form-control" />
                      <label htmlFor="picture_url">Address</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create</button>
              </form>
              <div className={messageClasses} id="success-message">
                Customer added!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default CustomerForm;
