import { useEffect, useState } from 'react';

function TechnicianList() {
  const [technicians, setTechnicians] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8080/api/technicians/');

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians)
    }
  }

  useEffect(()=>{
    getData()
  }, [])

//   const handleDelete = async (id) => {
//     await fetch(`http://localhost:8080/api/technicians/${id}/`, {method: "DELETE"})
//     await getData()
//   }

  return (
    <>
    <h1>Hello </h1>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Employee ID</th>
        </tr>
      </thead>
      <tbody>
        {technicians.map((technician, idx) => {
          return (
            <tr>
              <td>{ technician.first_name }</td>
              <td>{ technician.last_name }</td>
              <td>{ technician.employee_id}</td>

            </tr>

          );
        })}
      </tbody>
    </table>
    </>
  );
}

export default TechnicianList;
