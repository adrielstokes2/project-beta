import React, {useState, useEffect } from 'react';


function AppointmentForm() {
  const [technicians, setTechnicians] = useState([])
  const [formData, setFormData] = useState({
        vin: '',
        customer: '',
        date_time: '',
        technician:'',
        reason: ''
  

  })

  const [hasAppointment, setHasAppointment] = useState(false)

  const getData = async () => {
    const url = `http://localhost:8080/api/technicians/`
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  }
  
  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    
    const locationUrl = `http://localhost:8080/api/appointments/`;
    
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    
    if (response.ok) {
      setFormData({
        vin: '',
        customer: '',
        date_time: '',
        technician:'',
        reason: ''
      });

      setHasAppointment(true);
    }
  }

  const handleChangeName = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value 
    });
  }

  //Allows for the form to display on when a user has not signed in.
  //If a user has signed in, the css class d-none is added to the unneeded piece of the interface

   const formClasses = (!hasAppointment) ? '' : 'd-none';
   const messageClasses = (!hasAppointment) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';
  
  return (
    <div className="my-5">
      <div className="row">
        {/* <div className="col col-sm-auto">
          <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="logo.svg" />
        </div> */}

        <div className="col">
          <div className="card shadow">
            <div className="card-body">

              <form className={formClasses} onSubmit={handleSubmit} id="create-appointment-form">
                <h1 className="card-title">Make a service appointment</h1>
                <p className="mb-3">
                  Please choose a technician
                </p>
                
                <div className="mb-3">
                  <select onChange={handleChangeName} name="technician" id="technician" required>
                    <option value="">choose technician</option>
                    {
                      technicians.map(technician => {
                        return (
                          <option key={technician.href} value={technician.href}> {technician.employee_id}</option> //id?
                        )
                      })
                    }
                  </select>
                </div>

                <p className="mb-3">
                  Enter appointment details.
                </p>

                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.vin} required placeholder="VIN" type="text" id="vin" name="vin" className="form-control" />
                      <label htmlFor="vin">VIN</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.customer} required placeholder="customer" type="text" id="customer" name="customer" className="form-control" />
                      <label htmlFor="customer">customer</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.date_time} required placeholder="date time" type="text"  id="date_time" name="date_time" className="form-control" />
                      <label htmlFor="date_time">date time</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.reason} required placeholder="reason" type="text"  id="reason" name="reason" className="form-control" />
                      <label htmlFor="reason">Reason</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create</button>
              </form>
              

              <div className={messageClasses} id="success-message">
                Appointment set!
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AppointmentForm;
