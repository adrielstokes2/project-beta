import { useEffect, useState } from 'react';

function ListSalespeople() {
    const [salespeople, setSalespeople] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salesperson)
        }
    }

    useEffect(()=>{
        getData()
    }, [])


    return (
    <>
        <h1>Salespeople</h1>
        <table className="table table-striped">
          <thead>
            <tr>
                <th>Employee ID</th>
                <th>First Name</th>
                <th>Last Name</th>
            </tr>
          </thead>
          <tbody>
            {salespeople.map((salesperson, idx) => {
              return (
                <tr>
                    <td>{ salesperson.employee_id }</td>
                    <td>{ salesperson.first_name }</td>
                    <td>{ salesperson.last_name }</td>
                </tr>

              );
            })}
          </tbody>
        </table>
    </>
    );
}

export default ListSalespeople;
