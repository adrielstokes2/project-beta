import React, {useState, useEffect } from 'react';

function SalespersonForm() {

  const [formData, setFormData] = useState({
    first_name: '',
    last_name: '',
    employee_id: ''
  })
  const [hasSalesperson, setHasSalesperson] = useState(false)

  const handleSubmit = async (event) => {
    event.preventDefault();
    const locationUrl = `http://localhost:8090/api/salespeople/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      setFormData({
        first_name: '',
        last_name: '',
        employee_id: ''
      });
      setHasSalesperson(true);
    }
  }
  const handleChangeName = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    });
  }
  //Allows for the form to display on when a user has not signed in.
  //If a user has signed in, the css class d-none is added to the unneeded piece of the interface
   const formClasses = (!hasSalesperson) ? '' : 'd-none';
   const messageClasses = (!hasSalesperson) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';
  return (
    <div className="my-5">
      <div className="row">
        {/* <div className="col col-sm-auto">
          <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="logo.svg" />
        </div> */}
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-technician-form">
                <h1 className="card-title">It's auto Time!</h1>
                <p className="mb-3">
                  Please add a salesperson.
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.first_name} required placeholder="first name" type="text" id="first_name" name="first_name" className="form-control" />
                      <label htmlFor="model_name">First name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.last_name} required placeholder="last name" type="text" id="last_name" name="last_name" className="form-control" />
                      <label htmlFor="color">Last name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.employee_id} required placeholder="employee id" type="text"  id="employee_id" name="employee_id" className="form-control" />
                      <label htmlFor="picture_url">Employee ID</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create</button>
              </form>
              <div className={messageClasses} id="success-message">
                Salesperson added!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default SalespersonForm;
