import { useEffect, useState } from 'react';

function AppointmentList() {
  const [appointments, setAppointments] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8080/api/appointments/');

    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments)
    }
  }

  useEffect(()=>{
    getData()
  }, [])
  
//   const handleDelete = async (id) => { 
//     await fetch(`http://localhost:8080/api/technicians/${id}/`, {method: "DELETE"})
//     await getData()
//   }

const handleAppointmentCancel = async (id) => { 
    await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {method: "PUT"})
    await getData()
  }

  const handleAppointmentFinish = async (id) => { 
    await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, {method: "PUT"})
    await getData()
  }
  
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Customer</th>
          <th>Date and Time</th>
          <th>Reason</th>
          <th>Technician</th>
          <th>VIN</th>
          <th>VIP Status</th>
        </tr>
      </thead>
      <tbody>
        {appointments.map((appointment, idx) => {
          return (
            <tr key={appointment.id}>
            
              <td>{ appointment.customer }</td>
              <td>{ appointment.date_time }</td>
              <td>{ appointment.reason}</td>
              <td>{ appointment.technician.first_name}</td>
              <td>{ appointment.vin}</td>
              <td className="text-warning">{(appointment.vip_status ? "No" : null)}</td>
              <td><button className="btn btn-danger" onClick={() => handleAppointmentCancel(appointment.id)}>CANCEL</button></td>
              <td><button className="btn btn-success" onClick={() => handleAppointmentFinish(appointment.id)}>FINISH</button></td>


              
            </tr>
            
          );
        })}
      </tbody>
    </table>
  );
}

export default AppointmentList;




