from django.shortcuts import render
from .models import AutomobileVO, Sale, Salesperson, Customer
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
# Create your views here.
# what to do with ID??????
class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
        "id"
    ]

class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id"
    ]

class SalespersonDetailEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id"
    ]

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "phone_number",
        "address",
        "id"
    ]

class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "phone_number",
        "address",
        "id"
    ]

class SaleListEncoder(ModelEncoder):
    model = Sale
    properties = {
        "price",
        "automobile",
        "salesperson",
        "customer",
        "id"
    }
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonListEncoder(),
        "customer": CustomerListEncoder(),
    }

#end encoders
##salespeople views:::

@require_http_methods(["GET", "POST"])
def api_list_salesperson(request):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.all()
            return JsonResponse(
                {"salesperson": salesperson},
                encoder=SalespersonListEncoder,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Salesperson list does not exist"})
    else:
        try:
            content =json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonDetailEncoder,
                safe=False
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Cannot create salesperson"})


@require_http_methods(["GET", "DELETE", "POST"])
def api_show_salesperson(request, id): # really is the function to delete
    if request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(id=id)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalespersonDetailEncoder,
                safe=False
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Salesperson does not exist"})

##end salespeople views
#begin customers views:


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        try:
            customer = Customer.objects.all()
            return JsonResponse(
                {"customer": customer},
                encoder=CustomerListEncoder,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer list does not exist"})
    else:
        try:
            content =json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerDetailEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Can't create customer"})


@require_http_methods(["GET", "DELETE", "POST"])
def api_show_customers(request, id):
    if request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerDetailEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer does not exist"})


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        try:
            sales = Sale.objects.all()
            return JsonResponse(
                {"sales": sales},
                encoder=SaleListEncoder,
            )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Sales do not exist"})
    else:
        content = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            if automobile.sold == True:
                return JsonResponse({
                    "message": "Sorry this car is already sold"
                }, status = 400,)
            else:
                content["automobile"] = automobile
                salesperson = Salesperson.objects.get(id=content["salesperson"])

                content["salesperson"] = salesperson
                customer = Customer.objects.get(id=content["customer"])

                content["customer"] = customer

                sale = Sale.objects.create(**content)
                automobile.sold = True
                automobile.save()

                return JsonResponse(
                    sale,
                    encoder=SaleListEncoder,
                    safe=False
                )
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"messafe": "Invalid automobile id"},
                status=400
            )

@require_http_methods(["DELETE"])
def api_delete_sale(request, id):
    if request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=id)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SaleListEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Sale does not exist"})
