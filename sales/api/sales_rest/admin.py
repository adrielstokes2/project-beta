from django.contrib import admin

# Register your models here.
from .models import Sale, Salesperson, AutomobileVO, Customer

admin.site.register(Sale),
admin.site.register(Salesperson),
admin.site.register(AutomobileVO),
admin.site.register(Customer),
